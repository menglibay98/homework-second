<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>All Authors</title>
</head>
<body>
    <h1>All Authors</h1>

    <h1>Список авторов</h1>
    <table border="1">
        <thead>
        <tr>
            <th>ID</th>
            <th>Имя</th>
            <th>Фамилия</th>
            <th>Дата рождения</th>
            <th>Национальность</th>
            <th>Книги</th>
        </tr>
        </thead>
        <tbody>
        @foreach($authors as $author)
            <tr>
                <td>{{ $author->id }}</td>
                <td>{{ $author->firstName }}</td>
                <td>{{ $author->secondName }}</td>
                <td>{{ $author->birthDate }}</td>
                <td>{{ $author->nationality }}</td>
                <td>
                    <ul>
                        @foreach($author->books as $book)
                            <li>{{ $book->title }} ({{ $book->publication }})</li>
                        @endforeach
                    </ul>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</body>
</html>
