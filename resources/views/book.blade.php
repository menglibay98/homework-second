<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Books</title>
</head>
<body>
<h1>Список книг</h1>
<table border="1">
    <thead>
    <tr>
        <th>ID</th>
        <th>Название</th>
        <th>Дата публикации</th>
        <th>Жанр</th>
        <th>Автор</th>
    </tr>
    </thead>
    <tbody>
    @foreach($books as $book)
        <tr>
            <td>{{ $book->id }}</td>
            <td>{{ $book->title }}</td>
            <td>{{ $book->publication }}</td>
            <td>{{ $book->genre }}</td>
            <td>{{ $book->author->firstName }} {{ $book->author->secondName }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
</body>
</html>
