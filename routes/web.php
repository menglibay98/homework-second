<?php

use App\Http\Controllers\AuthorController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/author', AuthorController::class . '@index');
Route::get('/book', \App\Http\Controllers\BookController::class . '@books');
