<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use mysql_xdevapi\Table;

class Author extends Model
{
    use HasFactory;
//    public $id;
//    public $firstName;
//    public $secondName;
//
//    public $birthDate;
//    public $nationality;
//
//    protected $table = 'authors';

    protected $fillable = [
        'firstName',
        'secondName',
        'birthDate',
        'nationality',
    ];

    public function books(){
        return $this->hasMany(Book::class);
    }


}
