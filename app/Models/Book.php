<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;
//    public $id;
//    public $title;
//    public $publication;
//    public $genre;
//    public $author_id;
//
//    protected $table = 'books';

    protected $fillable = [
        'title',
        'publication',
        'genre',
        'author_id',
    ];

    public function author(){
        return $this->belongsTo(Author::class);
    }
}
