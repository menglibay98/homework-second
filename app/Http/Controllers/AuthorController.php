<?php

namespace App\Http\Controllers;

use App\Models\Author;
use Illuminate\Http\Request;

class AuthorController extends Controller
{
    public function index(){

//        $authors = Author::all();

        $authors = Author::with('books')->get();

        return view('author', compact('authors'));
    }
}
